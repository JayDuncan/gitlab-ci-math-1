#AP Assignment 1
# Accept 3 numbers
NUM1=$1
NUM2=$2
NUM3=$3

if [ -z ${OUTPUT_FILE_NAME} ] # returns true if variable  OUTPUT_FILE_NAME is not defined
then
#defines variable with default value
OUTPUT_FILE_NAME=“output.txt”
echo “Output file called ‘$OUTPUT_FILE_NAME’ created.”
else
echo “Use original file called ‘$OUTPUT_FILE_NAME’.”
fi
echo “Let’s do math….”
echo “$NUM1^$NUM2+$NUM3*$NUM2”
#Result= ($NUM1^$NUM2+$NUM3*$NUM2)
Result=$NUM1^$NUM2+$NUM3*$NUM2
#calc=“Here we go: $NUM1^$NUM2+"$NUM3"*"$NUM2"=$Result”
calc="$NUM1^$NUM2+$NUM3*$NUM2=$Result"
echo $calc
#Echo $calc > myresult.txt